<?php
/**
 * Created by PhpStorm.
 * User: wsalazar
 * Date: 3/30/15
 * Time: 5:58 PM
 */

namespace Asseter\UserBundle\Form;

use Asseter\UserBundle\Service\UserEmailRegisterService;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Form\Handler\RegistrationFormHandler as BaseHandler;
use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;


class RegistrationFormHandler extends BaseHandler
{
    protected $_em;

    public function __construct(FormInterface $form, Request $request,
                                UserManagerInterface $userManager, MailerInterface $mailer,
                                TokenGeneratorInterface $tokenGenerator, EntityManager $em)
    {
        parent::__construct($form, $request, $userManager, $mailer, $tokenGenerator);
        $this->_em = $em;
    }

    /**
     * @param boolean $confirmation
     */
    public function process($confirmation = false)
    {
        $user = $this->createUser();
        $this->form->setData($user);
        if ('POST' === $this->request->getMethod()) {
            $this->form->bind($this->request);
            if ($this->form->isValid()) {
                $this->onSuccess($user, $confirmation);

                return true;
            }
        }

        return false;
    }

    /**
     * @param boolean $confirmation
     */
    protected function onSuccess(UserInterface $user, $confirmation)
    {
        parent::onSuccess($user, $confirmation);

        $user->setFirstName($this->form->get('firstName')->getData());
        $user->setLastName($this->form->get('lastName')->getData());
        $user->setGender($this->form->get('gender')->getData());
        $user->setVerificationCode(uniqid(''));
        $user->addRole('ROLE_ADMIN');
        $this->_em->persist($user);
        $this->_em->flush();
    }

}