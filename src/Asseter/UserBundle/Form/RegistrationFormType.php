<?php
/**
 * Created by PhpStorm.
 * User: wsalazar
 * Date: 3/29/15
 * Time: 10:19 AM
 */

namespace Asseter\UserBundle\Form;

use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName', 'text', array('attr' => array('class' =>'form-control', 'placeholder' => 'First Name')));
        $builder->add('lastName', 'text', array('attr' => array('class' =>'form-control', 'placeholder' => 'Last Name')));
        $builder->add('gender', 'choice', array('choices' => array('Male','Female'),
                                                'expanded' => true));
        $builder->add('username', null, array('translation_domain' => 'FOSUserBundle',
                                              'attr' => array('class' =>'form-control', 'placeholder' => 'Username')));
        $builder->add('email', 'email', array('translation_domain' => 'FOSUserBundle',
                                              'attr' => array('class' =>'form-control', 'placeholder' => 'Email: ',)));
        $builder->add('plainPassword', 'repeated', array('type' => 'password',
                                                         'options' => array('translation_domain' => 'FOSUserBundle'),
                                                         'first_options' => array('label' => 'Password'),
                                                         'second_options' => array('label' => 'Repeat Password: '),
                                                         'invalid_message' => 'Passwords do not match.'));
    }
    public function getName()
    {
        return 'asseter_user_registration';
    }

    public function getParent()
    {
        return 'fos_user_registration';
    }

}