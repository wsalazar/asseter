<?php

namespace Asseter\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AsseterUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
