<?php
/**
 * Created by PhpStorm.
 * User: wsalazar
 * Date: 3/30/15
 * Time: 8:08 PM
 */

namespace Asseter\UserBundle\Service;


use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\DependencyInjection\Container;

class UserEmailRegisterService
{
    public function prepareFields(UserInterface $user, Container $container)
    {
        $from = $container->getParameter('mailer_user');
        $to = $user->getEmail();
        $subject = $user->getFirstName() . ', you have registered an account with Asseter.';
        $verificationCode = $user->getVerificationCode();
        $body = ['firstName'=>$user->getFirstName(), 'verificationCode'=>$verificationCode, 'host'=>$container->getParameter('router.request_context.host'), 'myEmail'=>$from];
        return ['from'=>$from,'to'=>$to,'subject'=>$subject,'body'=>$body];
    }
    /**
     * @param array $parts
     * @param array $body
     * @return \Swift_Mime_SimpleMimeEntity
     */
    public function sendVerificationEmailToUser($userFields, $mailer, $body)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject($userFields['subject'])
            ->setFrom($userFields['from'])
            ->setTo($userFields['to'])
            ->setContentType('text/html')
            ->setBody($body);
        $mailer->send($message);
    }
}